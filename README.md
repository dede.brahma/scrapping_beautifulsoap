# scrapping_beautifulsoap

Example scrapping web using [Beautifulsoap](https://pypi.org/project/beautifulsoup4/)

>  pip install beautifulsoup4

`from bs4 import BeautifulSoup`

`soup = BeautifulSoup(page.content, 'html.parser')`