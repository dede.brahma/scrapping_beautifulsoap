import requests
import csv
import json


url = ('https://newsapi.org/v2/everything?'
        'q=Natuna&'
        'from=2020-01-08&'
        'sortBy=popularity&'
        'language=id&'
        'apiKey=aaca8e17c89243efa5e6106555444110')
response = requests.get(url)
data = response.json()
data = data['articles']


path = open("./news_data.csv", "w")
writer = csv.writer(path)
for item in data:
    author = item['source']['name']
    writer.writerow([author, item['content']])